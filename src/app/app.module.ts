import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AngularFirestoreModule } from "@angular/fire/firestore";
import { AngularFireAuthModule } from "@angular/fire/auth";
import { AngularFireStorageModule } from "@angular/fire/storage";
import { AngularFireModule } from '@angular/fire';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { environment } from 'src/environments/environment';
import { Store } from './shared/store/store';
import { WrappersModule } from './main/wrappers/wrappers.module';
import { JoinModule } from './components/join/join.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { URL as DATABASE_URL } from '@angular/fire/database';
import { SETTINGS as FIRESTORE_SETTINGS } from '@angular/fire/firestore';
import { NgxIntlTelInputModule } from 'ngx-intl-tel-input';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AgmCoreModule } from '@agm/core';


@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireAuthModule,
    AngularFirestoreModule,
    AngularFireStorageModule,
    AgmCoreModule.forRoot({
      apiKey: environment.apiKey,
      libraries: ['places']
    }),
    JoinModule,
    WrappersModule,
    NgxIntlTelInputModule,
    NgbModule
  ],
  providers: [
    Store,
    {
      provide: DATABASE_URL,
      useValue: location.hostname === 'localhost' ? `http://localhost:9000?ns=${environment.firebase.projectId}` : undefined
    },
    { provide: FIRESTORE_SETTINGS, useValue: location.hostname === 'localhost' ? { host: 'localhost:8080', ssl: false } : {} },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
