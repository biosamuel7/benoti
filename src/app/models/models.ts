import * as firebase from "firebase/app";
import 'firebase/firestore'

export interface School extends Heritage {
    schoolId: string;
    schoolName: string;
    schoolNumber: string;
    schoolEmail: string;
    profileURL?: Storage;
    joinURL?: string;
    token?: string;
    billingActive?: Billing;
    welcomeMessage: string;
    birthDayMessage: string;
}

export interface Billing extends Heritage {
    docId?: string
    typeAccount: TypeAccount;
    textCount: number;
    mediaCount: number;
    isActive: boolean;
    expiredAt?: firebase.firestore.Timestamp;
}

export enum TypeAccount {
    PROFESSIONAL = "professional",
    BUSINESS = "business",
    ENTERPRISE = "enterprise"
}

export interface Formula {
    textCount: number
    mediaCount: number
    price: number
}

export interface Message extends Heritage {
    messageId?: string;
    messageBody: string;
    destinataires: Member[];
    delivers?: Deliver[];
    schoolId?: string;
    classrooms: string[];
    isFile?: boolean;
    file?: DocumentFile;
}

export interface DocumentUploaded {
    path: string,
    downloadURL: string
}

export interface Deliver {
    id: string;
    number: string;
    user?: string;
    sentAt?: string;
    processedAt?: string,
    deliverAt?: string,
}


export enum TypeGroup {
    PRIVATE = "PRIVATE",
    PUBLIC = "PUBLIC",
}

export interface ClassRoom extends Heritage {
    id?: string;
    name: string;
    schoolId: string;
    visibility: TypeGroup;
    description?: string;
}

export interface Member extends Heritage {
    id?: string;
    name: string;
    lastName: string;
    classrooms?: UserClassrooms[];
    phone?: Phone;
    email?: string;
    schoolId: string;
    profileURL?: Storage;
}

export interface DocumentFile {
    file?: File;
    size?: number;
    isUplaoded?: boolean;
    isBigSize?: boolean;
    isBadExtension?: boolean;
    name?: string;
    storage?: Storage;
}

export interface Phone {
    dialCode: string;
    number: string;
    internationalNumber: string;
    countryCode: string;
    nationalNumber: string;
    e164Number: string;
}

export interface UserClassrooms {
    classroomId: string
    name: string;
    joinedAt: string
}

export interface Sort {
    text?: string;
    propriete: string;
    action: string;
    value?: string | boolean;
}


export interface BloomMember extends Heritage {
    id?: string;
    accordParent?: string;
    birthday: string;
    classrooms?: UserClassrooms[];
    dateIntegration: string;
    email: string;
    gender: string;
    lastName: string;
    isGem: string;
    name: string; 
    phone: Phone;
    parent1: ParentBloom;
    parent2: ParentBloom;
    quartier: GMSPlace;
    responsable: string;
    schoolId: string;
    profileURL: Storage;
    profile: string;
    file?: DocumentFile;
}

interface ParentBloom {
    nom: string;
    phone: Phone;
}
  
export interface GMSPlace {
    name: string;
    placeId: string;
    longitude: number;
    latitude: number;
    url: string;
    formatted_address: string;
}


interface Heritage {
    createdAt?: firebase.firestore.Timestamp;
    updatedAt?: firebase.firestore.Timestamp;
}

export interface Storage {
    downloadURL: string,
    filePath: string
}
