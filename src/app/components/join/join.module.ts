import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ClassroomChooseComponent } from './step/classroom-choose/classroom-choose.component';
import { NumberAddComponent } from './step/number-add/number-add.component';
import { UserInfosComponent } from './step/user-infos/user-infos.component';
import { JoinComponent } from './join.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BasicComponent } from './basic/basic.component';
import { BloomComponent } from './bloom/bloom.component';
import { JoinRoutingModule } from './join-routing.module';
import { UserInfosBloomComponent } from './step/user-infos-bloom/user-infos-bloom.component';
import { NgxIntlTelInputModule } from 'ngx-intl-tel-input';
import { BloomSecDepartementComponent } from './step/bloom-sec-departement/bloom-sec-departement.component';
import { BloomSecParentComponent } from './step/bloom-sec-parent/bloom-sec-parent.component';



@NgModule({
  declarations: [ClassroomChooseComponent, NumberAddComponent, UserInfosComponent, JoinComponent, BasicComponent, BloomComponent, UserInfosBloomComponent, BloomSecDepartementComponent, BloomSecParentComponent],
  imports: [
    CommonModule,
    JoinRoutingModule,
    ReactiveFormsModule,
    NgxIntlTelInputModule,
    FormsModule
  ]
})
export class JoinModule { }
