import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BasicComponent } from './basic/basic.component';
import { BloomComponent } from './bloom/bloom.component';
import { JoinComponent } from './join.component';


const routes: Routes = [{
  path: '', component: JoinComponent,
  children: [
      {
          path: '',
          component: BasicComponent
      },
      {
          path: 'bloom',
          component: BloomComponent
      }
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class JoinRoutingModule { }
