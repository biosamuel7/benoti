import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable, iif, of } from 'rxjs';
import { switchMap, takeWhile } from 'rxjs/operators';
import { School, Member, ClassRoom, UserClassrooms, Phone } from 'src/app/models/models';
import { MemberService } from 'src/app/services/member/member.service';
import { SchoolService } from 'src/app/services/school/school.service';

@Component({
  selector: 'app-basic',
  templateUrl: './basic.component.html',
  styleUrls: ['./basic.component.scss']
})
export class BasicComponent implements OnInit {

  schoolConnected$: Observable<School>

  step: number = 1;

  member: Member;

  constructor(
    private route: ActivatedRoute,
    private service: SchoolService,
    private memberService: MemberService
  ) { }

  ngOnInit(): void {

    this.schoolConnected$ = this.route.queryParamMap.pipe(
      switchMap((params) => iif(() => !!params.get('school'), of(params.get('school')), of(undefined))),
      takeWhile(params => !!params),
      switchMap(params => this.service.getById(params))
      )
  }

  previous(step: number) {
    this.step = step
  }

  next(step: number){
    this.step = step
  }

  classRoom(classroom: ClassRoom) {
    const classr: UserClassrooms = {
      classroomId: classroom.id, 
      joinedAt: new Date(Date.now()).toString(), 
      name: classroom.name
    }

    this.member = {...this.member, classrooms: [classr]}
    console.log(this.member)
  }

  userInfo(infos: Member) {
    this.member = {...this.member, ...infos}
    console.log(this.member)
    this.finish()
  }

  userNumber(phone: Phone){
    this.member = {...this.member, phone}
    console.log(this.member)
  }

  finish(){
    this.route.queryParamMap.subscribe((params) => {
      this.memberService.add({...this.member, schoolId: params.get('school')}).then(result => {
        console.log(result)
        this.step = -1
      })
    })
    
  }


}
