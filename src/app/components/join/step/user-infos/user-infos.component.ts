import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CountryISO } from 'ngx-intl-tel-input';
import { Member } from 'src/app/models/models';

@Component({
  selector: 'join-user-infos',
  templateUrl: './user-infos.component.html',
  styleUrls: ['./user-infos.component.scss']
})
export class UserInfosComponent implements OnInit {
  
  @Output()
  previous: EventEmitter<Number> = new EventEmitter<Number>()

  @Output()
  next: EventEmitter<Number> = new EventEmitter<Number>()

  @Output()
  infos: EventEmitter<Member> = new EventEmitter<Member>()

  @Input()
  step: number;

  form: FormGroup;

  constructor(
    private fb: FormBuilder
  ) { }

  ngOnInit(): void {

    this.form = this.fb.group({
      name: ['',[
        Validators.required
      ]],
      lastName: ['',[
        Validators.required
      ]],
      birthDay: [this.convertDate(new Date()),[
        Validators.required
      ]],
      phone: ['',[
        Validators.required
      ]],
      gender: ['masculin', [Validators.required]]
    })

  }

  convertDate(inputFormat) {
    function pad(s) { return (s < 10) ? '0' + s : s; }
    var d = new Date(inputFormat)
    return [d.getFullYear(), pad(d.getMonth()+1),pad(d.getDate())].join('-')
  }

  OnNext(){
    this.infos.emit(this.form.value as Member)
    this.next.emit(++this.step)
  }

  OnPrevious(){
    this.previous.emit(--this.step)
  }

  get name () { return this.form.get('name') }
  get lastName () { return this.form.get('lastName') }
  get birthDay () { return this.form.get('birthDay') }
  get phone () { return this.form.get('phone') }
}
