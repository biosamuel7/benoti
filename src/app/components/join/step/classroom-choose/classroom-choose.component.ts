import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { ClassRoom, Member, TypeGroup } from 'src/app/models/models';
import { ClassroomService } from 'src/app/services/classroom/classroom.service';

@Component({
  selector: 'join-classroom-choose',
  templateUrl: './classroom-choose.component.html',
  styleUrls: ['./classroom-choose.component.scss']
})
export class ClassroomChooseComponent implements OnInit {

  @Output()
  previous: EventEmitter<Number> = new EventEmitter<Number>()

  @Output()
  next: EventEmitter<Number> = new EventEmitter<Number>()


  @Output()
  selected: EventEmitter<ClassRoom> = new EventEmitter<ClassRoom>()

  @Input()
  step: number;

  @Input()
  member: Member;

  @Input()
  schoolId: string;

  datas$: Observable<ClassRoom[]>

  classrooms: ClassRoom[] = []

  isSelected: boolean = false

  classRoomSelected: ClassRoom;

  indexSelected: number = 0;

  constructor(
    private service: ClassroomService
  ) { }

  ngOnInit(): void {
    this.datas$ = this.service.getBySchool(this.schoolId).pipe(
      tap(datas => {
        this.classrooms = datas.filter(classroom => classroom.visibility === TypeGroup.PUBLIC)
        console.log(this.classrooms)
        this.classRoomSelected = !!this.member ? this.classrooms.find(classroom => classroom.id === this.member.classrooms[0].classroomId) : datas[0]
        this.indexSelected = !!this.member ? this.classrooms.findIndex(classroom => classroom.id === this.member.classrooms[0].classroomId) : 0
      })
    )
  }

  OnNext(){
    this.selected.emit(this.classRoomSelected)
    this.next.emit(++this.step)
  }

  OnPrevious(){
    this.previous.emit(this.step > 2 ? --this.step : this.step)
  }

  onChecked(id: string){
    this.isSelected = true
    this.classRoomSelected = this.classrooms.find(clas => clas.id === id)
    // this.selected.emit(this.classrooms.find(clas => clas.id === id))
  }

}
