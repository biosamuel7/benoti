import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'join-number-add',
  templateUrl: './number-add.component.html',
  styleUrls: ['./number-add.component.scss']
})
export class NumberAddComponent implements OnInit {

  @Output()
  previous: EventEmitter<Number> = new EventEmitter<Number>()

  @Output()
  next: EventEmitter<Number> = new EventEmitter<Number>()

  @Output()
  finish: EventEmitter<true> = new EventEmitter<true>()

  @Input()
  step: number;

  constructor() { }

  ngOnInit(): void {
  }

  OnNext(){
    this.next.emit(++this.step)
  }

  OnPrevious(){
    this.previous.emit(--this.step)
  }

  go(){
    this.finish.emit(true)
  }

}
