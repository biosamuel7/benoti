import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BloomSecParentComponent } from './bloom-sec-parent.component';

describe('BloomSecParentComponent', () => {
  let component: BloomSecParentComponent;
  let fixture: ComponentFixture<BloomSecParentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BloomSecParentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BloomSecParentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
