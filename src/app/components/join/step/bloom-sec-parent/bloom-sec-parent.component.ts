import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CountryISO } from 'ngx-intl-tel-input';
import { first, map, tap } from 'rxjs/operators';
import { BloomMember, Member } from 'src/app/models/models';

@Component({
  selector: 'bloom-sec-parent',
  templateUrl: './bloom-sec-parent.component.html',
  styleUrls: ['./bloom-sec-parent.component.scss']
})
export class BloomSecParentComponent implements OnInit {

  @Output()
  previous: EventEmitter<Number> = new EventEmitter<Number>()

  @Output()
  next: EventEmitter<Number> = new EventEmitter<Number>()

  @Output()
  infos: EventEmitter<BloomMember> = new EventEmitter<BloomMember>()

  @Input()
  step: number;

  @Input()
  member: BloomMember;

  form: FormGroup;

  CountryISO = CountryISO;

  constructor(
    private fb: FormBuilder
  ) { }

  ngOnInit(): void {

    this.form = this.fb.group({
      responsable: ['les_2_parents',[
        Validators.required
      ]],
      accordParent: ['',[
        Validators.required
      ]],
      parent1: this.fb.group({
        nom: ['', [Validators.required]],
        phone: ['',[Validators.required]]
      }),
      parent2:  this.fb.group({
        nom: ['', [Validators.required]],
        phone: ['',[Validators.required]]
      })
    })
   

    this.responsable.valueChanges
    // .pipe(first())
    .subscribe(val => {
      console.log(val)
      if(val === 'les_2_parents') {
        this.parent2.get('nom').setValidators(Validators.required)
        this.parent2.get('phone').setValidators(Validators.required)
      } else {
        this.parent2.get('nom').clearValidators()
        this.parent2.get('phone').clearValidators()
      }

      this.parent2.reset()
      this.parent1.reset()
    })

  }

  convertDate(inputFormat) {
    function pad(s) { return (s < 10) ? '0' + s : s; }
    var d = new Date(inputFormat)
    return [d.getFullYear(), pad(d.getMonth()+1),pad(d.getDate())].join('-')
  }

  OnNext(){
    this.infos.emit({...this.member, ...this.form.value})
    this.next.emit(++this.step)
  }

  OnPrevious(){
    this.previous.emit(--this.step)
  }

  get parent1 () { return this.form.get('parent1') as FormGroup }
  get parent2 () { return this.form.get('parent2') as FormGroup }
  get responsable() { return this.form.get('responsable')}

}
