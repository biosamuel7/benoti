import { Component, EventEmitter, Input, OnInit, Output, Sanitizer, NgZone, ElementRef, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { DomSanitizer } from '@angular/platform-browser';
import { CountryISO } from 'ngx-intl-tel-input';
import { BloomMember, DocumentFile, GMSPlace, Member } from 'src/app/models/models';
import { MapsAPILoader } from '@agm/core';

@Component({
  selector: 'join-user-infos-bloom',
  templateUrl: './user-infos-bloom.component.html',
  styleUrls: ['./user-infos-bloom.component.scss']
})
export class UserInfosBloomComponent implements OnInit {

   
  @Output()
  previous: EventEmitter<Number> = new EventEmitter<Number>()

  @Output()
  next: EventEmitter<Number> = new EventEmitter<Number>()

  @Output()
  infos: EventEmitter<Member> = new EventEmitter<Member>()

  @Input()
  step: number;

  @Input()
  member: BloomMember;

  form: FormGroup;

  CountryISO = CountryISO;

  url_img = "assets/images/blank-profile.png"


  @ViewChild('place',{static: false})
  public searchElement: ElementRef;

  constructor(
    private fb: FormBuilder,
    private sanitize: DomSanitizer,
    private mapAPILoader: MapsAPILoader,
    private ngZone: NgZone,
  ) { }

  ngOnInit(): void {

    this.form = this.fb.group({
      name: ['',[
        Validators.required
      ]],
      lastName: ['',[
        Validators.required
      ]],
      birthDay: [this.convertDate(new Date()),[
        Validators.required
      ]],
      phone: ['',[
        Validators.required
      ]],
      email: ['',[
        Validators.required,
        Validators.email
      ]],
      gender: ['masculin',[
        Validators.required
      ]],
      quartier: [,[Validators.required]],
      profile: ['',[Validators.required]]
    })

    if(this.member && !!this.member.name){
      console.log(this.member)
      this.name.setValue(this.member.name)
      this.lastName.setValue(this.member.lastName)
      this.email.setValue(this.member.email)
      this.phone.setValue(this.member.phone)
      this.gender.setValue(this.member.gender)
      this.birthDay.setValue(this.member.birthday)
      this.quartier.setValue(this.member.quartier)
    }


    this.mapAPILoader.load().then(() => {
      this.setZone(this.searchElement);
    });

  }

  convertDate(inputFormat) {
    function pad(s) { return (s < 10) ? '0' + s : s; }
    var d = new Date(inputFormat)
    return [d.getFullYear(), pad(d.getMonth()+1),pad(d.getDate())].join('-')
  }

  OnNext(){
    this.infos.emit({...this.form.value, file: this.filePicked[0]} as BloomMember)
    this.next.emit(++this.step)
  }

  OnPrevious(){
    this.previous.emit(--this.step)
  }

  filePicked: DocumentFile[] = []
  onPickFile($event: FileList) {
    console.log($event)
    if ($event.item(0) === null) { return }
    const file = $event[0]
    const size = Math.ceil(file.size / 1000000)
    const isBigSize = file.size > 10000000;
    const isBadExtension = !["pdf","doc","docx",'jpeg','png'].includes(file.name.split('.').pop())
    const document: DocumentFile = {file: $event[0], size: size, isBigSize: isBigSize, isBadExtension: isBadExtension, isUplaoded: false, name: file.name}
    this.filePicked.splice(0, 1, document);

    const reader = new FileReader();
    reader.onload = (e: any) => {
      this.profile.setValue(e.target.result)
    };
    reader.readAsDataURL($event[0]);
  }

  /**
   * set autocomplete google map place in inputs
   * @param element 
   */
  private setZone(element: ElementRef){
    //set options for autocomplete maps
    const options = {
      types:['geocode','establishment']
    }

    const autoComplete =  new google.maps.places.Autocomplete(element.nativeElement,options);
    autoComplete.addListener('place_changed',() => {
      this.ngZone.run(() => {
        let place = autoComplete.getPlace();
    
        if (place.geometry == undefined || place.geometry == null) {
          return;
        }
        let coordinate =  place.geometry.location;
        const data = {name: place.name, url: place.url, formatted_address: place.formatted_address, longitude: coordinate.lng(), latitude: coordinate.lat(), placeId: place.place_id} as GMSPlace;
        this.quartier.setValue(data);
      })
    });
  }

  removeFile(){
    this.filePicked = []
    this.profile.reset()
  }

  showImage() {
    return this.sanitize.bypassSecurityTrustUrl(this.profile.value)
  }

  get name () { return this.form.get('name') }
  get lastName () { return this.form.get('lastName') }
  get birthDay () { return this.form.get('birthDay') }
  get phone () { return this.form.get('phone') }
  get email() { return this.form.get('email')}
  get quartier() { return this.form.get('quartier')}
  get gender() { return this.form.get('gender')}
  get profile() { return this.form.get('profile')}

}
