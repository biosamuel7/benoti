import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserInfosBloomComponent } from './user-infos-bloom.component';

describe('UserInfosBloomComponent', () => {
  let component: UserInfosBloomComponent;
  let fixture: ComponentFixture<UserInfosBloomComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserInfosBloomComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserInfosBloomComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
