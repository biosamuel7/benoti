import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BloomSecDepartementComponent } from './bloom-sec-departement.component';

describe('BloomSecDepartementComponent', () => {
  let component: BloomSecDepartementComponent;
  let fixture: ComponentFixture<BloomSecDepartementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BloomSecDepartementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BloomSecDepartementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
