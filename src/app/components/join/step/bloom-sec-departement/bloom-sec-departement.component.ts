import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Observable, Subscription } from 'rxjs';
import { tap } from 'rxjs/operators';
import { BloomMember, ClassRoom, TypeGroup, UserClassrooms } from 'src/app/models/models';
import { ClassroomService } from 'src/app/services/classroom/classroom.service';

@Component({
  selector: 'bloom-sec-departement',
  templateUrl: './bloom-sec-departement.component.html',
  styleUrls: ['./bloom-sec-departement.component.scss']
})
export class BloomSecDepartementComponent implements OnInit {

  @Output()
  previous: EventEmitter<Number> = new EventEmitter<Number>()

  @Output()
  next: EventEmitter<Number> = new EventEmitter<Number>()


  @Output()
  selected: EventEmitter<BloomMember> = new EventEmitter<BloomMember>()

  @Input()
  step: number;

  @Input()
  schoolId: string;

  @Input()
  member: BloomMember;

  indexSelected: number = 0;

  datas$: Observable<ClassRoom[]>

  classrooms: ClassRoom[] = []

  isSelected: boolean = false;

  form: FormGroup;

  hasDepartementSub: Subscription;

  constructor(
    private service: ClassroomService,
    private fb: FormBuilder
  ) { }

  ngOnDestroy(): void {
    this.hasDepartementSub.unsubscribe()
  }

  ngOnInit(): void {
    console.log(this.schoolId)
   

    this.form = this.fb.group({
      dateIntegration: [this.convertDate(new Date()),[Validators.required]],
      classRoomSelected: ['',[Validators.required]],
      hasDepartement: ['non',[Validators.required]],
      inGem: ['non',[Validators.required]]
    })

    this.hasDepartementSub = this.hasDepartement.valueChanges.subscribe(val => {
      this.dateIntegration.setValue(this.convertDate(new Date()))
    })

    this.datas$ = this.service.getBySchool(this.schoolId).pipe(
      tap(datas => {
        this.classrooms = datas.filter(classroom => classroom.visibility === TypeGroup.PUBLIC)
        this.classRoomSelected.setValue(this.classrooms[0].id)
      })
    )
  }

  OnNext(){
    const select = this.classrooms.find(elt => elt.id === this.classRoomSelected.value)

    this.member = {...this.member, classrooms:[
      {
        classroomId: select.id, 
        joinedAt: new Date(Date.now()).toString(), 
        name: select.name
      } as UserClassrooms
    ], dateIntegration: this.dateIntegration.value, isGem: this.inGem.value}

    this.selected.emit(this.member)
    this.next.emit(++this.step)
  }

  OnPrevious(){
    this.previous.emit(this.step > 2 ? --this.step : this.step)
  }
  
  convertDate(inputFormat) {
    function pad(s) { return (s < 10) ? '0' + s : s; }
    var d = new Date(inputFormat)
    return [d.getFullYear(), pad(d.getMonth()+1),pad(d.getDate())].join('-')
  }


  get hasDepartement(){ return this.form.get('hasDepartement')}
  get classRoomSelected(){ return this.form.get('classRoomSelected')}
  get dateIntegration(){ return this.form.get('dateIntegration')}
  get inGem(){ return this.form.get('inGem')}

}
