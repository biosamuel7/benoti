import { Component, OnInit } from '@angular/core';
import { AngularFireStorage } from '@angular/fire/storage';
import { ActivatedRoute } from '@angular/router';
import { Observable, iif, of, from, pipe } from 'rxjs';
import { map, mergeMap, switchMap, take, takeWhile, tap, toArray } from 'rxjs/operators';
import { School, Member, BloomMember, DocumentFile, Storage } from 'src/app/models/models';
import { MemberService } from 'src/app/services/member/member.service';
import { SchoolService } from 'src/app/services/school/school.service';

@Component({
  selector: 'app-bloom',
  templateUrl: './bloom.component.html',
  styleUrls: ['./bloom.component.scss']
})
export class BloomComponent implements OnInit {

  schoolConnected$: Observable<School>

  step: number = 1;

  member: BloomMember;

  constructor(
    private route: ActivatedRoute,
    private service: SchoolService,
    private memberService: MemberService,
    private storage: AngularFireStorage
  ) { }

  ngOnInit(): void {

    this.schoolConnected$ = this.route.queryParamMap.pipe(
      switchMap((params) => iif(() => !!params.get('school'), of(params.get('school')), of(undefined))),
      takeWhile(params => !!params),
      switchMap(params => this.service.getById(params))
      )
  }

  previous(step: number) {
    this.step = step
  }

  next(step: number){
    this.step = step
  }

  classRoom(data: BloomMember) {
    this.member = {...this.member, ...data}
  }

  userInfo(infos: BloomMember) {
    this.member = {...this.member, ...infos}
    console.log('info',this.member)
  }

  async finish(infos: BloomMember){
    // this.member = infos
    console.log('classs',this.member)
    console.log('infos',infos)

    this.member = {...infos}

    var parents: Member[] = [];

    if(this.member.responsable === "les_2_parents") {
      parents.push( {
        name: this.member.name, 
        lastName: this.member.parent2.nom,
        phone: this.member.parent2.phone, 
        classrooms: [{
        classroomId: '5bvpej8at3dyfOkCZ4FZ',
        joinedAt: new Date().toString(),
        name: 'Parents',
        }],
        schoolId: 'gzFOUOB32WfRYl0Q4ITOi52GHK82'
      })

    }

    parents.push({
      name: this.member.name, 
      lastName: this.member.parent1.nom,
      phone: this.member.parent1.phone, 
      classrooms: [{
      classroomId: '5bvpej8at3dyfOkCZ4FZ',
      joinedAt: new Date().toString(),
      name: 'Parents',
      }],
      schoolId: 'gzFOUOB32WfRYl0Q4ITOi52GHK82'
    })

    console.log('send',this.member)
    const file = this.member.file;
    const split = file.name.split('.')
    const filePath = `FILES/${split.shift()}__${Date.now().toString()}.${split.pop()}`;
    const task = await this.storage.upload(filePath, file.file);

    const getDownloadURL = () => {
      return this.storage.ref(filePath).getDownloadURL().pipe(map(url => {
        return {...file, filePath, downloadURL: url, isUplaoded: true} as DocumentFile
      }));
    }


    from(parents)
    .pipe(mergeMap(parent => of(this.memberService.add(parent))),take(parents.length),toArray(), tap(rest => console.log('save parents:',rest)))
    .pipe(switchMap(() => this.storage.ref(filePath).getDownloadURL()))
    .pipe(switchMap((url => {
      return of({...file, filePath, downloadURL: url, isUplaoded: true} as Storage)
    })))
    .pipe(switchMap((doc: Storage) => {
      console.log('file',doc)
      delete doc['file']
      delete this.member.file['file']
      return this.memberService.add({...this.member, schoolId: 'gzFOUOB32WfRYl0Q4ITOi52GHK82', profileURL: doc})
    }))
    .subscribe(rest => {
      console.log('rest end',rest)
      this.step = -1
    }, err => console.error(err))
    
  }

}