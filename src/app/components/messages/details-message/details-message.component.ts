import { Route } from '@angular/compiler/src/core';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import { first, map, switchMap, tap } from 'rxjs/operators';
import { Deliver, Message } from 'src/app/models/models';
import { MessageService } from 'src/app/services/message/message.service';

@Component({
  selector: 'app-details-message',
  templateUrl: './details-message.component.html',
  styleUrls: ['./details-message.component.scss']
})
export class DetailsMessageComponent implements OnInit {

  message$: Observable<Message>

  delivers: Deliver[] = []

  message: Message

  page = 1;
  pageSize = 6;

  constructor(
    private service: MessageService,
    private route: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.message$ = this.route
    .paramMap
    .pipe(
      map(params => params.get('id')),
      switchMap(id => this.service.getById(id)),
      tap(message => {
        this.message = message
        this.refreshDataPagination()
      })
    )
  }

  refreshDataPagination() {
    if(!!this.message.delivers) {
      this.delivers = this.message.delivers
        .slice((this.page - 1) * this.pageSize, (this.page - 1) * this.pageSize + this.pageSize);
    }
  }

}
