import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MessagesRoutingModule } from './messages-routing.module';
import { WriteMessageComponent } from './write-message/write-message.component';
import { ListMessagesComponent } from './list-messages/list-messages.component';
import { MessagesComponent } from './messages.component';
import { ReactiveFormsModule } from '@angular/forms';
import { DetailsMessageComponent } from './details-message/details-message.component';
import { SharedModule } from 'src/app/shared/shared.module';


@NgModule({
  declarations: [
    WriteMessageComponent, 
    ListMessagesComponent, 
    MessagesComponent, 
    DetailsMessageComponent,
  ],
  imports: [
    CommonModule,
    MessagesRoutingModule,
    ReactiveFormsModule,
    SharedModule
  ]
})
export class MessagesModule { }
