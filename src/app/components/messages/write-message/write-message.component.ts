import { Component, OnInit } from '@angular/core';
import { AngularFireStorage } from '@angular/fire/storage';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DomSanitizer } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { of, zip } from 'rxjs';
import { finalize, first, map, switchMap, take } from 'rxjs/operators';
import { ClassRoom, DocumentFile, DocumentUploaded, Member, Storage } from 'src/app/models/models';
import { ClassroomService } from 'src/app/services/classroom/classroom.service';
import { MemberService } from 'src/app/services/member/member.service';
import { MessageService } from 'src/app/services/message/message.service';

declare var $: any;

@Component({
  selector: 'main-write-message',
  templateUrl: './write-message.component.html',
  styleUrls: ['./write-message.component.scss']
})
export class WriteMessageComponent implements OnInit {

  messageForm: FormGroup;

  classrooms: ClassRoom[] = []

  members: Member[] = []

  constructor(
    private fb: FormBuilder,
    private classroomService: ClassroomService,
    private msgService: MessageService,
    private memberSerive: MemberService,
    private router: Router,
    private stani: DomSanitizer,
    private storage: AngularFireStorage
  ) { }

  ngOnInit(): void {

    let tab = new Set<ClassRoom>()

    zip(this.classroomService.get(), this.memberSerive.get())
    .pipe(first())
    .subscribe(([classrooms, members]) => {
      this.members = members
      members.forEach(member => {
        classrooms.forEach(classroom => {
          if(!!member.classrooms.find(classr => classr.classroomId === classroom.id)){
            tab.add(classroom)
          }
        })
      })
      
      this.classrooms = [...tab]
    })


    this.messageForm = this.fb.group({
      messageBody: ['Bonjour {{NAME}}, \n',[
        Validators.required
      ]],
      destinataires: ['', [
        Validators.required
      ]]
    })
    
  }

  async onSubmit() {

    let dest = new Set<Member>()
    const classrooms: string [] = this.destinataires.value
    classrooms.forEach(id => {
      this.members.forEach(member => {
        if(!!member.classrooms.find(classr => classr.classroomId === id)) {
          dest.add({
            name: member.name,
            lastName: member.lastName,
            phone: member.phone
          } as Member)
        }
      })
    })

    if(!!this.filePicked.length) {
      const file = this.filePicked[0];
      const split = file.name.split('.')
      const filePath = `FILES/${split.shift()}__${Date.now().toString()}.${split.pop()}`;
      const task = await this.storage.upload(filePath, file.file);

      const getDownloadURL = () => {
        return this.storage.ref(filePath).getDownloadURL().pipe(map(url => {
          return {...file, filePath, downloadURL: url, isUplaoded: true} as DocumentFile
        }));
      }

      getDownloadURL()
      .pipe(switchMap(uploaded =>  {
        delete uploaded['file']
        console.log(uploaded)
        return this.msgService.add({
          messageBody: this.messageBody.value,
          destinataires: [...dest],
          classrooms: this.destinataires.value,
          isFile: true,
          file: uploaded
        })
      }
      )).pipe(first())
      .subscribe((result) => {
        result.then(res => {
          this.router.navigate(['messages/details',res.id])
        })
        .catch(err => console.error(err))
      }, err => console.log(err))
      // .subscribe(res => console.log(res))



    }else{
      this.msgService.add({
        messageBody: this.messageBody.value,
        destinataires: [...dest],
        classrooms: this.destinataires.value,
        isFile: false
      })
      .pipe(first())
      .subscribe((result) => {
        result.then(res => {
          this.router.navigate(['messages/details',res.id])
        })
        .catch(err => console.error(err))
      }, err => console.log(err))
    }

  }

  addName(){
    let msg = this.messageBody.value
    this.messageBody.setValue(msg+" {{NAME}} ")
  }

  addLastName(){
    let msg = this.messageBody.value
    this.messageBody.setValue(msg+" {{LASTNAME}} ")
  }

  showfile(file: string){
    return this.stani.bypassSecurityTrustResourceUrl(file)
  }

  filePicked: DocumentFile[] = []
  onPickFile($event: FileList) {
    console.log()
    if ($event.item(0) === null) { return }
    const file = $event[0]
    const size = Math.ceil(file.size / 1000000)
    const isBigSize = file.size > 10000000;
    const isBadExtension = !["pdf","doc","docx"].includes(file.name.split('.').pop())
    const document: DocumentFile = {file: $event[0], size: size, isBigSize: isBigSize, isBadExtension: isBadExtension, isUplaoded: false, name: file.name}
    this.filePicked.splice(0, 1, document);
  }

  removeFile(){
    this.filePicked = []
  }

  get messageBody () { return this.messageForm.get('messageBody') }
  get destinataires () { return this.messageForm.get('destinataires') }

}
