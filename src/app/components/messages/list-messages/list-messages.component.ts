import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { first, tap } from 'rxjs/operators';
import { Message } from 'src/app/models/models';
import { MessageService } from 'src/app/services/message/message.service';

@Component({
  selector: 'main-list-messages',
  templateUrl: './list-messages.component.html',
  styleUrls: ['./list-messages.component.scss']
})
export class ListMessagesComponent implements OnInit {

  // datas$: Observable<{messages: Message[],start: QueryDocumentSnapshot<DocumentData>, end: QueryDocumentSnapshot<DocumentData>}>;
  datas$: Observable<Message[]>;
  messages: Message[];
  paginatorData: Message[] =  []
  pageSize = 8;
  page = 1;
  



  constructor(
    private service: MessageService
  ) { }

  ngOnInit(): void {

    this.datas$ = this.service.get().pipe(
      first(),
      tap(datas => {
        this.messages = datas
        .sort((a, b) => {
          const date1 = a.createdAt.toDate()
          const date2 = b.createdAt.toDate()
          return date1.getTime() < date2.getTime() ? 1 : 0
        })
        this.refreshDataPagination()
    }))

  }

  refreshDataPagination() {
    this.paginatorData = this.messages
      .slice((this.page - 1) * this.pageSize, (this.page - 1) * this.pageSize + this.pageSize)
      .sort((a, b) => {
        return a.createdAt.toDate().getTime() < b.createdAt.toDate().getTime() ? 1 : -1
      });
  }

  // next(doc: QueryDocumentSnapshot<DocumentData>){
  //   this.datas$ = this.service.getNext(doc)
  // }

  // before(doc: QueryDocumentSnapshot<DocumentData>){
  //   this.datas$ = this.service.getBefore(doc)
  // }

}
