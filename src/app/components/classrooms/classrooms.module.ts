import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ClassroomsComponent } from './classrooms.component';
import { ClassroomInfosComponent } from './classroom-infos/classroom-infos.component';
import { RouterModule } from '@angular/router';
import { AddClassroomComponent } from './add-classroom/add-classroom.component';
import { ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from 'src/app/shared/shared.module';
import { ShowMembersComponent } from './show-members/show-members.component';
import { ListClassroomComponent } from './list-classroom/list-classroom.component';
import { ClassroomsRoutingModule } from './classrooms-routing.module';



@NgModule({
  declarations: [
    ClassroomsComponent,
    ClassroomInfosComponent,
    AddClassroomComponent,
    ShowMembersComponent,
    ListClassroomComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    ClassroomsRoutingModule,
    ReactiveFormsModule,
    SharedModule
  ]
})
export class ClassroomsModule { }
