import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ClassRoom } from 'src/app/models/models';
import { ClassroomService } from 'src/app/services/classroom/classroom.service';

@Component({
  selector: 'classroom',
  templateUrl: './classroom-infos.component.html',
  styleUrls: ['./classroom-infos.component.scss']
})
export class ClassroomInfosComponent implements OnInit {

  @Input() 
  classroom: ClassRoom;
  
  constructor(
    private service: ClassroomService,
    private router: Router
  ) { }

  ngOnInit(): void {
  }

  delete(id: string){
    // this.service.d(id)
    // .then(res => console.log(res))
    // .catch(err => console.error(err))
  }


  show(){
    this.router.navigate(['classrooms/members',this.classroom.id])
  }

}
