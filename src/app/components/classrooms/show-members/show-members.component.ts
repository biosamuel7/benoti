import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { forkJoin, Observable, pipe } from 'rxjs';
import { first, map, switchMap, tap } from 'rxjs/operators';
import { ClassRoom, Member, UserClassrooms } from 'src/app/models/models';
import { ClassroomService } from 'src/app/services/classroom/classroom.service';
import { MemberService } from 'src/app/services/member/member.service';

@Component({
  selector: 'app-show-members',
  templateUrl: './show-members.component.html',
  styleUrls: ['./show-members.component.scss']
})
export class ShowMembersComponent implements OnInit {

  data$: Observable<{users: Member[], classroom: ClassRoom}>;

  page = 1;
  pageSize = 6;

  users: Member[] = [];

  constructor(
    private route: ActivatedRoute,
    private service: MemberService,
    private classRoomService: ClassroomService
  ) { }

  ngOnInit(): void {

    this.data$ = this.route.paramMap
    .pipe(
      map(params => params.get('id')),
      switchMap(id => forkJoin(
        {
          users: this.service.get().pipe(
          first(),
          map(users => users.filter(user => !!user.classrooms.find(classR => classR.classroomId === id)))),
          classroom: this.classRoomService.getById(id).pipe(first())
        }
      )
      )
    )
    .pipe(
      first(),
      tap(({users, classroom}) => {
        this.users = users
        this.refreshDataPagination(users)
      })
    ) 
    // .subscribe(({users, classroom}) => console.log(users))

  }

  getDate(classrooms: UserClassrooms[], id: string){
    return classrooms.find(classroom => classroom.classroomId === id)
  }

  refreshDataPagination(data: Member[]) {
    if(!!this.users.length) {
      this.users = data
        .slice((this.page - 1) * this.pageSize, (this.page - 1) * this.pageSize + this.pageSize);
    }
  }

  annuler(){

  }

  save(){
    
  }

}
