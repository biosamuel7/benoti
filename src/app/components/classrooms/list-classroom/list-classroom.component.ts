import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { ClassRoom } from 'src/app/models/models';
import { ClassroomService } from 'src/app/services/classroom/classroom.service';

@Component({
  selector: 'app-list-classroom',
  templateUrl: './list-classroom.component.html',
  styleUrls: ['./list-classroom.component.scss']
})
export class ListClassroomComponent implements OnInit {

  datas$: Observable<ClassRoom[]>;

  constructor(
    private service: ClassroomService
  ) { }

  ngOnInit(): void {
   this.datas$ = this.service.get()
  }

}
