import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ClassroomsComponent } from './classrooms.component';
import { ListClassroomComponent } from './list-classroom/list-classroom.component';
import { ShowMembersComponent } from './show-members/show-members.component';

const routes: Routes = [{
  path: '', component: ClassroomsComponent,
  children: [
      {
          path: '',
          component: ListClassroomComponent
      },
      {
          path: 'members/:id',
          component: ShowMembersComponent
      }
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ClassroomsRoutingModule { }
