import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { ClassRoom } from 'src/app/models/models';
import { ClassroomService } from 'src/app/services/classroom/classroom.service';

@Component({
  selector: 'main-classrooms',
  templateUrl: './classrooms.component.html',
  styleUrls: ['./classrooms.component.scss']
})
export class ClassroomsComponent implements OnInit {

  datas$: Observable<ClassRoom[]>;

  constructor(
    private service: ClassroomService
  ) { }

  ngOnInit(): void {
   this.datas$ = this.service.get()
  }

}
