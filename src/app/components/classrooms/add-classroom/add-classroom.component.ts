import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';
import { TypeGroup } from 'src/app/models/models';
import { ClassroomService } from 'src/app/services/classroom/classroom.service';

declare var $: any;

@Component({
  selector: 'add-classroom',
  templateUrl: './add-classroom.component.html',
  styleUrls: ['./add-classroom.component.scss']
})
export class AddClassroomComponent implements OnInit {

  @Output() 
  added: EventEmitter<string> = new EventEmitter<string>();

  classroomForm: FormGroup;

  get name () { return this.classroomForm.get('name')}
  get typegroup () { return this.classroomForm.get('typegroup')}
  get description () { return this.classroomForm.get('description')}

  constructor(
    private fb: FormBuilder,
    private service: ClassroomService,
  ) { }

  ngOnInit(): void {

    this.classroomForm = this.fb.group({
      name: ['', [
        Validators.required
      ]],
      typegroup: [TypeGroup.PRIVATE, [Validators.required]],
      description: ['', []]
    })
    
  }

  onSave(){
    this.service.add({name: this.name.value, visibility: this.typegroup.value, description: this.description.value})
    .pipe(first())
    .subscribe(result => {
      this.added.emit(this.name.value)
      this.classroomForm.reset()
      $('#addClassroom').modal('toggle')
    })
    
  }

}
