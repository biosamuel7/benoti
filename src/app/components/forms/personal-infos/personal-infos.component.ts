import { MapsAPILoader } from '@agm/core';
import { Component, ElementRef, NgZone, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { DomSanitizer } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { CountryISO } from 'ngx-intl-tel-input';
import { BloomMember, DocumentFile, GMSPlace } from 'src/app/models/models';
import { CustomValidators } from 'src/app/shared/customValidators';
import { Store } from 'src/app/shared/store/store';

@Component({
  selector: 'app-personal-infos',
  templateUrl: './personal-infos.component.html',
  styleUrls: ['./personal-infos.component.scss']
})
export class PersonalInfosComponent implements OnInit {


  member: BloomMember;

  form: FormGroup;

  CountryISO = CountryISO;

  url_img = "assets/images/blank-profile.png"


  @ViewChild('place',{static: false})
  public searchElement: ElementRef;

  constructor(
    private fb: FormBuilder,
    private sanitize: DomSanitizer,
    private mapAPILoader: MapsAPILoader,
    private router: Router,
    private ngZone: NgZone,
    private store: Store
  ) { }

  ngOnInit(): void {

    this.form = this.fb.group({
      name: ['',[
        Validators.required
      ]],
      lastName: ['',[
        Validators.required
      ]],
      birthDay: [this.convertDate(new Date()),[
        Validators.required
      ]],
      phone: ['',[
        Validators.required
      ]],
      email: ['',[
        Validators.required,
        Validators.email
      ]],
      gender: ['masculin',[
        Validators.required
      ]],
      // quartier: [,[Validators.required]],
      // profile: ['',[Validators.required]]
    })

    this.store.select<BloomMember>('bloomer')
    .subscribe(member => {
      if(member){
        this.member = member
        this.name.setValue(this.member.name)
        this.lastName.setValue(this.member.lastName)
        this.email.setValue(this.member.email)
        this.phone.setValue(this.member.phone)
        this.gender.setValue(this.member.gender)
        this.birthDay.setValue(this.convertDate(this.member.birthday))
        // this.profile.setValue(this.member.profile)
        // this.quartier.setValue(this.member.quartier.name)
      }
    })
   


    // this.mapAPILoader.load().then(() => {
    //   this.setZone(this.searchElement);
    // });

  }
  
  public nextStep() {
    const data = {...this.form.value, schoolId: 'gzFOUOB32WfRYl0Q4ITOi52GHK82'};
    this.store.set('bloomer', data);
    this.router.navigate(['/forms/parent-infos'])
  }

  convertDate(inputFormat) {
    function pad(s) { return (s < 10) ? '0' + s : s; }
    var d = new Date(inputFormat)
    return [d.getFullYear(), pad(d.getMonth()+1),pad(d.getDate())].join('-')
  }

  filePicked: DocumentFile[] = []
  onPickFile($event: FileList) {
    console.log($event)
    if ($event.item(0) === null) { return }
    const file = $event[0]
    const size = Math.ceil(file.size / 1000000)
    const isBigSize = file.size > 10000000;
    const isBadExtension = !["pdf","doc","docx",'jpeg','png'].includes(file.name.split('.').pop())
    const document: DocumentFile = {file: $event[0], size: size, isBigSize: isBigSize, isBadExtension: isBadExtension, isUplaoded: false, name: file.name}
    this.filePicked.splice(0, 1, document);

    const reader = new FileReader();
    reader.onload = (e: any) => {
      this.profile.setValue(e.target.result)
    };
    reader.readAsDataURL($event[0]);
  }

  /**
   * set autocomplete google map place in inputs
   * @param element 
   */
  private setZone(element: ElementRef){
    //set options for autocomplete maps
    const options = {
      types:['geocode','establishment']
    }

    const autoComplete =  new google.maps.places.Autocomplete(element.nativeElement,options);
    autoComplete.addListener('place_changed',() => {
      this.ngZone.run(() => {
        let place = autoComplete.getPlace();
    
        if (place.geometry == undefined || place.geometry == null) {
          return;
        }
        let coordinate =  place.geometry.location;
        const data = {name: place.name, url: place.url, formatted_address: place.formatted_address, longitude: coordinate.lng(), latitude: coordinate.lat(), placeId: place.place_id} as GMSPlace;
        this.quartier.setValue(data);
      })
    });
  }

  removeFile(){
    this.filePicked = []
    this.profile.reset()
  }

  showImage() {
    return this.sanitize.bypassSecurityTrustUrl(this.profile.value)
  }

  get name () { return this.form.get('name') }
  get lastName () { return this.form.get('lastName') }
  get birthDay () { return this.form.get('birthDay') }
  get phone () { return this.form.get('phone') }
  get email() { return this.form.get('email')}
  get gender() { return this.form.get('gender')}
  get profile() { return this.form.get('profile')}
  get quartier() { return this.form.get('quartier')}

}
