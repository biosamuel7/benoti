import { MemberService } from 'src/app/services/member/member.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { from, Observable, of } from 'rxjs';
import { map, mergeMap, switchMap, take, tap, toArray } from 'rxjs/operators';
import { BloomMember, ClassRoom, DocumentFile, Member, TypeGroup, UserClassrooms, Storage } from 'src/app/models/models';
import { ClassroomService } from 'src/app/services/classroom/classroom.service';
import { Store } from 'src/app/shared/store/store';
import { AngularFireStorage } from '@angular/fire/storage';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-departement-infos',
  templateUrl: './departement-infos.component.html',
  styleUrls: ['./departement-infos.component.scss']
})
export class DepartementInfosComponent implements OnInit {
  
  member: BloomMember;

  isFormLoading: boolean = false;

  indexSelected: number = 0;

  datas$: Observable<ClassRoom[]>

  classrooms: ClassRoom[] = []

  isSelected: boolean = false;

  form: FormGroup;

  constructor(
    private router: Router,
    private service: ClassroomService,
    private memberService: MemberService,
    private storage: AngularFireStorage,
    private store: Store,
    private fb: FormBuilder
  ) { }

  ngOnInit(): void {

    this.form = this.fb.group({
      dateIntegration: [this.convertDate(new Date()),[Validators.required]],
      classRoomSelected: ['',[Validators.required]],
      hasDepartement: ['non',[Validators.required]],
      inGem: ['non',[Validators.required]]
    })

    this.hasDepartement.valueChanges.subscribe(val => {
      this.dateIntegration.setValue(this.convertDate(new Date()))
    })

    this.datas$ = this.store.select<BloomMember>('bloomer')
    .pipe(
      tap(bloomer => {
        this.member = bloomer;
      }),
      switchMap(bloomer => this.service.getBySchool(bloomer.schoolId)),
      tap(datas => {
        this.classrooms = datas.filter(classroom => classroom.visibility === TypeGroup.PUBLIC)
        this.classRoomSelected.setValue(this.classrooms[0].id)
    }))
    
  }

  OnNext(){
    const select = this.classrooms.find(elt => elt.id === this.classRoomSelected.value)

    this.member = {...this.member, classrooms:[
      {
        classroomId: select.id, 
        joinedAt: new Date(Date.now()).toString(), 
        name: select.name
      } as UserClassrooms
    ], dateIntegration: this.dateIntegration.value, isGem: this.inGem.value}
    this.finish()
  }

  previousStep() {
    this.router.navigate(['/forms/parent-infos'])
  }
  

  async finish(){
    this.isFormLoading = true;

    var parents: Member[] = [];

    if(this.member.responsable === "les_2_parents") {
      parents.push( {
        name: this.member.name, 
        lastName: this.member.parent2.nom,
        phone: this.member.parent2.phone, 
        classrooms: [{
        classroomId: '5bvpej8at3dyfOkCZ4FZ',
        joinedAt: new Date().toString(),
        name: 'Parents',
        }],
        schoolId: 'gzFOUOB32WfRYl0Q4ITOi52GHK82'
      })

    }

    parents.push({
      name: this.member.name, 
      lastName: this.member.parent1.nom,
      phone: this.member.parent1.phone, 
      classrooms: [{
      classroomId: '5bvpej8at3dyfOkCZ4FZ',
      joinedAt: new Date().toString(),
      name: 'Parents',
      }],
      schoolId: 'gzFOUOB32WfRYl0Q4ITOi52GHK82'
    })

    // const file = this.member.file;
    // const split = file.name.split('.')
    // const filePath = `FILES/${split.shift()}__${Date.now().toString()}.${split.pop()}`;
    // const task = await this.storage.upload(filePath, file.file);

    // const getDownloadURL = () => {
    //   return this.storage.ref(filePath).getDownloadURL().pipe(map(url => {
    //     return {...file, filePath, downloadURL: url, isUplaoded: true} as DocumentFile
    //   }));
    // }

    from(parents)
    .pipe(mergeMap(parent => of(this.memberService.add(parent))),take(parents.length),toArray(), 
    // tap(rest => console.log('save parents:',rest))
    )
    .pipe(switchMap(() => {
      // console.log('file',doc)
      // delete this.member.file['file']
      return this.memberService.add({...this.member, schoolId: 'gzFOUOB32WfRYl0Q4ITOi52GHK82'})
    }))
    .subscribe(rest => {
      if(environment.production) {
        console.log('rest end',rest)
      }
      this.isFormLoading = false
      this.router.navigate(['forms/success'])
    }, err => console.error(err))
    
    
  }

  uploadProfilPicture(parents: any[], filePath: string, file) {
    from(parents)
    .pipe(mergeMap(parent => of(this.memberService.add(parent))),take(parents.length),toArray(), 
    // tap(rest => console.log('save parents:',rest))
    )
    .pipe(switchMap(() => this.storage.ref(filePath).getDownloadURL()))
    .pipe(switchMap((url => {
      return of({...file, filePath, downloadURL: url, isUplaoded: true} as Storage)
    })))
    .pipe(switchMap((doc: Storage) => {
      // console.log('file',doc)
      delete doc['file']
      delete this.member.file['file']
      return this.memberService.add({...this.member, schoolId: 'gzFOUOB32WfRYl0Q4ITOi52GHK82', profileURL: doc})
    }))
    .subscribe(rest => {
      if(environment.production) {
        console.log('rest end',rest)
      }
      this.isFormLoading = false
      this.router.navigate(['forms/success'])
    }, err => console.error(err))
  }


  convertDate(inputFormat) {
    function pad(s) { return (s < 10) ? '0' + s : s; }
    var d = new Date(inputFormat)
    return [d.getFullYear(), pad(d.getMonth()+1),pad(d.getDate())].join('-')
  }


  get hasDepartement(){ return this.form.get('hasDepartement')}
  get classRoomSelected(){ return this.form.get('classRoomSelected')}
  get dateIntegration(){ return this.form.get('dateIntegration')}
  get inGem(){ return this.form.get('inGem')}

}

