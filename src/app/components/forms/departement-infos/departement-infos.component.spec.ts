import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DepartementInfosComponent } from './departement-infos.component';

describe('DepartementInfosComponent', () => {
  let component: DepartementInfosComponent;
  let fixture: ComponentFixture<DepartementInfosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DepartementInfosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DepartementInfosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
