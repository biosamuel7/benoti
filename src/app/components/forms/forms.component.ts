import { Component, OnInit } from '@angular/core';
import { SchoolService } from 'src/app/services/school/school.service';

@Component({
  selector: 'app-forms',
  templateUrl: './forms.component.html',
  styleUrls: ['./forms.component.scss']
})
export class FormsComponent implements OnInit {

  schoolConnected$;

  constructor(
    private service: SchoolService
  ) { }

  ngOnInit(): void {
    this.schoolConnected$ = this.service.getById("gzFOUOB32WfRYl0Q4ITOi52GHK82")
  }

}
