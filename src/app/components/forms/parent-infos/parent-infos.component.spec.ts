import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ParentInfosComponent } from './parent-infos.component';

describe('ParentInfosComponent', () => {
  let component: ParentInfosComponent;
  let fixture: ComponentFixture<ParentInfosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ParentInfosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ParentInfosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
