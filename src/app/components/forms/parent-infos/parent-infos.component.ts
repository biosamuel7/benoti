import { first } from 'rxjs/operators';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { CountryISO } from 'ngx-intl-tel-input';
import { BloomMember } from 'src/app/models/models';
import { Store } from 'src/app/shared/store/store';

@Component({
  selector: 'app-parent-infos',
  templateUrl: './parent-infos.component.html',
  styleUrls: ['./parent-infos.component.scss']
})
export class ParentInfosComponent implements OnInit {

  member: BloomMember;

  form: FormGroup;

  CountryISO = CountryISO;

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private store: Store
  ) { }

  ngOnInit(): void {

    this.form = this.fb.group({
      responsable: ['les_2_parents',[
        Validators.required
      ]],
      accordParent: ['',[
        Validators.required
      ]],
      parent1: this.fb.group({
        nom: ['', [Validators.required]],
        phone: ['',[Validators.required]]
      }),
      parent2:  this.fb.group({
        nom: ['', [Validators.required]],
        phone: ['',[Validators.required]]
      })
    })
   
    this.store.select<BloomMember>('bloomer')
    .subscribe(member => {
      this.member = member;
      if(member.parent1 || member.parent2){
        this.responsable.setValue(member.responsable)
        this.form.get('accordParent').setValue(member.accordParent)
        this.parent1.setValue(member.parent1)
        this.parent2.setValue(member.parent2)
      }
    })

    this.responsable.valueChanges
    // .pipe(first())
    .subscribe(val => {
      console.log(val)
      if(val === 'les_2_parents') {
        this.parent2.get('nom').setValidators(Validators.required)
        this.parent2.get('phone').setValidators(Validators.required)
      } else {
        this.parent2.get('nom').clearValidators()
        this.parent2.get('phone').clearValidators()
      }

      this.parent2.reset()
      this.parent1.reset()
    })

  }

  convertDate(inputFormat) {
    function pad(s) { return (s < 10) ? '0' + s : s; }
    var d = new Date(inputFormat)
    return [d.getFullYear(), pad(d.getMonth()+1),pad(d.getDate())].join('-')
  }

  previousStep() {
    this.router.navigate(['/forms/personal-infos'])
  }

  public nextStep() {
    this.store.set('bloomer', {...this.member, ...this.form.value})
    this.router.navigate(['/forms/departement-infos'])
  }

  get parent1 () { return this.form.get('parent1') as FormGroup }
  get parent2 () { return this.form.get('parent2') as FormGroup }
  get responsable() { return this.form.get('responsable')}

}

