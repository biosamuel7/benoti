import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DepartementInfosComponent } from './departement-infos/departement-infos.component';
import { FormsComponent } from './forms.component';
import { ParentInfosComponent } from './parent-infos/parent-infos.component';
import { PersonalInfosComponent } from './personal-infos/personal-infos.component';
import { SuccessViewComponent } from './success-view/success-view.component';


const routes: Routes = [{
  path: '', component: FormsComponent,
  children: [
    {
      path: '', redirectTo: 'personal-infos'
    },
    {
      path: 'personal-infos', component: PersonalInfosComponent
    },
    {
      path: 'parent-infos', component: ParentInfosComponent
    },
    {
      path: 'departement-infos', component: DepartementInfosComponent
    },
    {
      path: 'success', component: SuccessViewComponent
    }
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FormsRoutingModule { }
