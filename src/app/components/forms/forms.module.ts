import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FormsRoutingModule } from './forms-routing.module';
import { FormsComponent } from './forms.component';
import { PersonalInfosComponent } from './personal-infos/personal-infos.component';
import { ParentInfosComponent } from './parent-infos/parent-infos.component';
import { DepartementInfosComponent } from './departement-infos/departement-infos.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { SuccessViewComponent } from './success-view/success-view.component';


@NgModule({
  declarations: [FormsComponent, PersonalInfosComponent, ParentInfosComponent, DepartementInfosComponent, SuccessViewComponent],
  imports: [
    CommonModule,
    FormsRoutingModule,
    SharedModule
  ]
})
export class FormsModule { }
