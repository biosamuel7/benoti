import { Component, OnInit } from '@angular/core';
import { forkJoin, Observable } from 'rxjs';
import { first, tap } from 'rxjs/operators';
import { Billing, Formula, School } from 'src/app/models/models';
import { SchoolService } from 'src/app/services/school/school.service';
import { Store } from 'src/app/shared/store/store';

@Component({
  selector: 'main-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  data$: Observable<{billing:Billing; formula:Formula, schoolConnected: School}>;

  isBillingExpired: boolean = true

  constructor(
    private service: SchoolService,
    private store: Store
  ) { }

  ngOnInit(): void {
    this.data$ = forkJoin(
      {
        billing: this.service.getActiveBilling().pipe(first()),
        formula: this.service.getSchoolFormula().pipe(first()),
        schoolConnected: this.store.select<School>('schoolConnected').pipe(first())
      } 
    ).pipe(tap(d => {
      console.log(d)
      let expiredAt = d.schoolConnected.billingActive.expiredAt.toDate().getTime()
      let now = new Date().getTime()

      this.isBillingExpired = expiredAt > now 

    }))
  }

  getPercentOfValue(value: number, total: number, withPercent: boolean = false){
    let percent = value * 100 / total
    return withPercent ? `${percent}%` : percent
  }

}
