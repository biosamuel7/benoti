import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Member } from 'src/app/models/models';
import { MemberService } from 'src/app/services/member/member.service';

@Component({
  selector: 'student-infos',
  templateUrl: './student-infos.component.html',
  styleUrls: ['./student-infos.component.scss']
})
export class StudentInfosComponent implements OnInit {

  @Input() member: Member

  constructor(
    private router: Router,
    private service: MemberService
  ) { }

  ngOnInit(): void {

  }

  deleteMember(id: string){
    this.service.deleteUserById(id)
    .then(res => console.log(res))
    .catch(err => console.error(err))
  }

  addMembber(id: string){
    console.log(id)
  }

  showGroups(id: string){
    this.router.navigate(['members/details',id])
  }

}
