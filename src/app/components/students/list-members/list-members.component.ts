import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { Member } from 'src/app/models/models';
import { MemberService } from 'src/app/services/member/member.service';

@Component({
  selector: 'app-list-members',
  templateUrl: './list-members.component.html',
  styleUrls: ['./list-members.component.scss']
})
export class ListMembersComponent implements OnInit {

  datas$: Observable<Member[]>
  members: Member[];
  paginatorData: Member[] =  []
  pageSize = 9;
  page = 1;

  constructor(
    private service: MemberService,
  ) { }

  ngOnInit(): void {
    this.datas$ = this.service.get().pipe(
      tap(datas => {
        this.members = datas
        this.refreshDataPagination()
      })
    )
  }

  refreshDataPagination() {
    this.paginatorData = this.members
      .slice((this.page - 1) * this.pageSize, (this.page - 1) * this.pageSize + this.pageSize);
  }

  memberSelected: string = ''
  getMemberId(id: string){
    this.memberSelected = id
  }

}
