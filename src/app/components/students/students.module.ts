import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { NgbPaginationModule } from '@ng-bootstrap/ng-bootstrap';
import { SharedModule } from 'src/app/shared/shared.module';
import { StudentInfosComponent } from './student-infos/student-infos.component';
import { StudentsComponent } from './students.component';
import { ShowGroupsComponent } from './show-groups/show-groups.component';
import { ListMembersComponent } from './list-members/list-members.component';
import { StudentsRoutingModule } from './students-routing.module';

@NgModule({
  declarations: [
    StudentInfosComponent,
    StudentsComponent,
    ShowGroupsComponent,
    ListMembersComponent,
  ],
  imports: [
    CommonModule,
    StudentsRoutingModule,
    NgbPaginationModule,
    SharedModule
  ]
})
export class StudentsModule { }
