import { NgModule } from '@angular/core';
import { StudentsComponent } from './students.component';
import { ListMembersComponent } from './list-members/list-members.component';
import { Routes, RouterModule } from '@angular/router';
import { ShowGroupsComponent } from './show-groups/show-groups.component';


const routes: Routes = [{
  path: '', component: StudentsComponent,
  children: [
    { path: '', component: ListMembersComponent},
    { path: 'details/:id', component: ShowGroupsComponent},
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class StudentsRoutingModule { }
