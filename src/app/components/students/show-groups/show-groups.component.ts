import { Location } from '@angular/common';
import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { first, map, switchMap } from 'rxjs/operators';
import { Member } from 'src/app/models/models';
import { ClassroomService } from 'src/app/services/classroom/classroom.service';
import { MemberService } from 'src/app/services/member/member.service';

@Component({
  selector: 'show-groups',
  templateUrl: './show-groups.component.html',
  styleUrls: ['./show-groups.component.scss']
})
export class ShowGroupsComponent implements OnInit {

  member$: Observable<Member>

  constructor(
    private service: MemberService,
    private location: Location,
    private route: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.member$ = this.route.paramMap
    .pipe(
      map(params => params.get('id')),
      switchMap(id => this.service.getById(id)),
      first()
    )
  }

  goBack(){
    this.location.back()
  }

}
