import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { first, map, tap } from 'rxjs/operators';
import { School } from 'src/app/models/models';
import { SchoolService } from 'src/app/services/school/school.service';
import { CustomValidators } from 'src/app/shared/customValidators';
import { Store } from 'src/app/shared/store/store';

declare var $:any

@Component({
  selector: 'app-personal',
  templateUrl: './personal.component.html',
  styleUrls: ['./personal.component.scss']
})
export class PersonalComponent implements OnInit {

  schoolConnected$: Observable<School>;

  school: School;

  infoSchoolForm: FormGroup;

  messageForm: FormGroup;

  constructor(
    private store: Store,
    private fb: FormBuilder,
    private service: SchoolService
  ) { }

  ngOnInit(): void {

    this.infoSchoolForm = this.fb.group({
      name: [, [
        Validators.required
      ]],
      email: [, [
        Validators.required
      ]],
      profileURl: [, [
        Validators.required
      ]],
      phone: [{value:'', disabled: true},[
        Validators.required
      ]]
    })

    this.messageForm = this.fb.group({
      welcomeMessage: [,[Validators.required]],
      birthDayMessage: [,[Validators.required]]
    })

    this.schoolConnected$ = this.store.select<School>('schoolConnected').pipe(
      first(),tap(school => this.setUpFormValue(school))
    )
  }

  setUpFormValue(school: School){
    console.log('school => ',school)
    this.school = school
    this.name.setValue(school.schoolName)
    this.email.setValue(school.schoolEmail)
    this.phone.setValue(school.schoolNumber)
    this.profileURL.setValue(school.profileURL.downloadURL)

    this.welcomeMessage.setValue(school.welcomeMessage)
    this.welcomeMessage.setValidators([
      Validators.required,
    ])

    this.birthDayMessage.setValue(school.birthDayMessage)
    this.birthDayMessage.setValidators([
      Validators.required,
    ])
  }

  saveMessage(){
    const data = {
      ...this.school, 
      birthDayMessage: this.birthDayMessage.value, 
      welcomeMessage: this.welcomeMessage.value
    } as School
    this.store.set('schoolConnected',data)

    this.service.updateSchool(data)
  }

  annulerMessage(){
    this.welcomeMessage.setValue(this.school.welcomeMessage)
    this.birthDayMessage.setValue(this.school.birthDayMessage)
  }

  get name() { return this.infoSchoolForm.get('name')}
  get email() { return this.infoSchoolForm.get('email')}
  get profileURL() { return this.infoSchoolForm.get('profileURl')}
  get phone() { return this.infoSchoolForm.get('phone')}

  get welcomeMessage() { return this.messageForm.get('welcomeMessage')}
  get birthDayMessage() { return this.messageForm.get('birthDayMessage')}
}

