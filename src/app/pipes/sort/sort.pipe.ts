import { Pipe, PipeTransform } from '@angular/core';
import { Sort } from 'src/app/models/models';

@Pipe({
  name: 'sort'
})
export class SortPipe implements PipeTransform {

  transform(data: any[], sort: Sort): any[] {
    return sort ? data.sort((a,b) => {
      switch (sort.action) {
        case "asc":
          if (a[sort.propriete] < b[sort.propriete]) { return -1}
          if (a[sort.propriete] > b[sort.propriete]) { return 1}
          break;
        case "desc":
          if (b[sort.propriete] < a[sort.propriete]) { return -1}
          if (b[sort.propriete] > a[sort.propriete]) { return 1}
          break;
        case "egale":
          if (a[sort.propriete] === sort.value) { return -1}
          break;
        case "date":
          const date1 = a[sort.propriete] as firebase.firestore.Timestamp
          const date2 = b[sort.propriete] as firebase.firestore.Timestamp
          if(date1.toDate().getTime() < date2.toDate().getTime()) { return -1}
          break;
        default:
          break;
      }
      
      return 0;
    }) : data;
  }
  
}
