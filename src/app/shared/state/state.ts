import { BloomMember, School } from 'src/app/models/models';

export interface State {
    school: School,
    bloomer: BloomMember
}