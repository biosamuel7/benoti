import { AbstractControl } from "@angular/forms"
import { of } from "rxjs"
import { SchoolService } from "../services/school/school.service"

export class CustomValidators {
    
   static checkMessageChanged(oldMessage: string) {
       return (control: AbstractControl) => {
           const message = control.value as string
           if(oldMessage.toLowerCase() !== message.toLowerCase()){
               return null
           } else {
               return {isDifferente: null}
           }
       }
   }
}
