import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SortPipe } from '../pipes/sort/sort.pipe';
import { NgbDropdownModule, NgbModalModule, NgbPaginationModule, NgbTooltipModule } from '@ng-bootstrap/ng-bootstrap';
import { NgSelectModule } from '@ng-select/ng-select';
import { ReactiveFormsModule } from '@angular/forms';
import { NgxIntlTelInputModule } from 'ngx-intl-tel-input';



@NgModule({
  declarations: [
    SortPipe
  ],
  imports: [
    CommonModule
  ],
  exports: [
    SortPipe,
    NgbDropdownModule,
    NgbTooltipModule,
    NgbPaginationModule,
    NgbModalModule,
    NgSelectModule,
    ReactiveFormsModule,
    NgxIntlTelInputModule
  ]
})
export class SharedModule { }
