import { BehaviorSubject, Observable } from 'rxjs';
import { distinctUntilChanged, pluck } from 'rxjs/operators';
import { State } from '../state/state';

const state:State = {
    school: undefined,
    bloomer: undefined
};

export class Store {

    private subject = new BehaviorSubject<State>(state);
    private store = this.subject.asObservable().pipe(distinctUntilChanged());

    get value(){ return this.subject.value;}

    /**
     * function permettant de recuperer la donnée store 
     * @param name Action
     */
    select<T>(name: string): Observable<T>{
        return this.store.pipe(pluck(name));
    }

    /**
     * function permettant de stocker des données en local
     * @param name STRING 
     * @param state ANY la valeur à enregistrer
     */
    set(name:string, state: any){
        this.subject.next({
            ...this.value,[name]: state
        })
    }
}