import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PersonalComponent } from '../components/account/personal/personal.component';
import { SecurityComponent } from '../components/account/security/security.component';
import { DashboardComponent } from '../components/dashboard/dashboard.component';
import { MainComponent } from './main.component';

const routes: Routes = [{
  path: '', component: MainComponent,
  children: [
    { path: '', redirectTo: 'dashboard', pathMatch: 'full'},
    { path: 'dashboard', component: DashboardComponent},
    { path: 'members', loadChildren: async() => (await import ('../components/students/students.module')).StudentsModule},
    { path: 'messages', loadChildren: async() => (await import ('../components/messages/messages.module')).MessagesModule},
    { path: 'classrooms', loadChildren: async() => (await import ('../components/classrooms/classrooms.module')).ClassroomsModule},
    { path: 'personal', component: PersonalComponent},
    { path: 'security', component: SecurityComponent},
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MainRoutingModule { }
