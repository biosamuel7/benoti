import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MainRoutingModule } from './main-routing.module';
import { MainComponent } from './main.component';
import { RouterModule } from '@angular/router';
import { StudentsModule } from '../components/students/students.module';
import { ClassroomsModule } from '../components/classrooms/classrooms.module';
import { ReactiveFormsModule } from '@angular/forms';
import { WrappersModule } from './wrappers/wrappers.module';
import { DashboardComponent } from '../components/dashboard/dashboard.component';
import { PersonalComponent } from '../components/account/personal/personal.component';
import { SecurityComponent } from '../components/account/security/security.component';
import { SharedModule } from '../shared/shared.module';


@NgModule({
  declarations: [
    MainComponent,
    DashboardComponent,
    PersonalComponent,
    SecurityComponent
  ],
  imports: [
    CommonModule,
    MainRoutingModule,
    RouterModule,
    StudentsModule,
    ClassroomsModule,
    ReactiveFormsModule,
    WrappersModule,
    SharedModule
  ]
})
export class MainModule { }
