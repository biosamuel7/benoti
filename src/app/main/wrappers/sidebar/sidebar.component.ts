import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { School } from 'src/app/models/models';
import { Store } from 'src/app/shared/store/store';

@Component({
  selector: 'main-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {


  schoolConnected$: Observable<School>;

  constructor(
    private store: Store
  ) { }

  ngOnInit(): void {
    this.schoolConnected$ = this.store.select<School>('schoolConnected')
  }

}
