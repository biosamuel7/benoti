import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { School } from 'src/app/models/models';
import { AuthService } from 'src/app/services/auth/auth.service';
import { Store } from 'src/app/shared/store/store';

@Component({
  selector: 'main-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  schoolConnected$: Observable<School>;

  constructor(
    private router: Router,
    private authService: AuthService,
    private store: Store
  ) { }

  ngOnInit(): void {
    this.schoolConnected$ = this.store.select<School>('schoolConnected')
  }

  logOut() {
    this.authService.logOut().then(res => this.router.navigate(['auth']))
  }

}
