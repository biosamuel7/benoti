import { Injectable } from '@angular/core';
import { AngularFirestore, DocumentData, QueryDocumentSnapshot } from '@angular/fire/firestore';

import * as firebase from 'firebase/app';
import "firebase/firestore";

import { Observable, of } from 'rxjs';
import { switchMap, map } from 'rxjs/operators';
import { School, Message } from 'src/app/models/models';
import { Store } from 'src/app/shared/store/store';

@Injectable({
  providedIn: 'root'
})
export class MessageService {

  constructor(
    private afs: AngularFirestore,
    private store: Store
  ) { 

  }

  get(): Observable<Message[]> {
    return this.store.select<School>('schoolConnected')
    .pipe(
      switchMap(school => this.afs.collection<Message>('messages', ref => 
      ref.where('schoolId','==',school.schoolId))
      .get()
      .pipe(
        map(queryDocs => {
          let messages: Message[] = []
          queryDocs.forEach(doc => {
            const data = doc.data()
            const messageId = doc.id
            messages.push({messageId, ...data} as Message)
          })
          return messages
        })
      ))
    )
  }

  getPagination(): Observable<{messages: Message[],start: QueryDocumentSnapshot<DocumentData>, end: QueryDocumentSnapshot<DocumentData>}> {
    return this.store.select<School>('schoolConnected')
    .pipe(
      switchMap(school => this.afs.collection<Message>('messages', ref => 
      ref.where('schoolId','==',school.schoolId)
      .limit(8))
      .get()
      .pipe(
        map(queryDocs => {
          let messages: Message[] = []
          queryDocs.forEach(doc => {
            const data = doc.data()
            const messageId = doc.id
            messages.push({messageId, ...data} as Message)
          })
          return {messages, start: queryDocs.docs.shift(), end: queryDocs.docs.pop()}
        })
      ))
    )
  }

  getNext(doc: QueryDocumentSnapshot<DocumentData>)
  : Observable<{messages: Message[],start: QueryDocumentSnapshot<DocumentData>, end: QueryDocumentSnapshot<DocumentData>}> {

    return this.store.select<School>('schoolConnected')
    .pipe(
      switchMap(school => this.afs.collection<Message>('messages', ref => 
      ref.where('schoolId','==',school.schoolId)
      .limit(8)
      .startAfter(doc))
      .get()
      .pipe(
        map(queryDocs => {
          let messages: Message[] = []
          queryDocs.forEach(doc => {
            const data = doc.data()
            const messageId = doc.id
            messages.push({messageId, ...data} as Message)
          })
          return {messages, start: queryDocs.docs.shift(), end: queryDocs.docs.pop()}
        })
      ))
    )
  }

  
  getBefore(doc: QueryDocumentSnapshot<DocumentData>)
  : Observable<{messages: Message[],start: QueryDocumentSnapshot<DocumentData>, end: QueryDocumentSnapshot<DocumentData>}> {

    return this.store.select<School>('schoolConnected')
    .pipe(
      switchMap(school => this.afs.collection<Message>('messages', ref => 
      ref.where('schoolId','==',school.schoolId)
      .limit(8)
      .endBefore(doc))
      .get()
      .pipe(
        map(queryDocs => {
          let messages: Message[] = []
          queryDocs.forEach(doc => {
            const data = doc.data()
            const messageId = doc.id
            messages.push({messageId, ...data} as Message)
          })
          return {messages, start: queryDocs.docs.shift(), end: queryDocs.docs.pop()}
        })
      ))
    )
  }

  
  getById(messageId: string): Observable<Message> {
    return this.store.select<School>('schoolConnected')
    .pipe(
      switchMap(school => this.afs.collection<Message>('messages', ref => ref.where('schoolId','==',school.schoolId))
      .doc<Message>(messageId)
      .valueChanges()
      .pipe(
        map(data => ({messageId, ...data} as Message))
      )
      ))
  }

  add(data: Message) {
    return this.store.select<School>('schoolConnected')
    .pipe(
      switchMap(school => of(this.afs.collection<Message>('messages').add({
        ...data, 
        schoolId: school.schoolId, 
        createdAt: this.timestamp, 
        updatedAt: this.timestamp} as Message))
    ))
  }

  createId(){
    return this.afs.createId();
  }

  get timestamp() {
    return firebase.firestore.FieldValue.serverTimestamp()
  }
}
