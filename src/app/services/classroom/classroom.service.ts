import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Observable, of } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';

import * as firebase from 'firebase/app';
import "firebase/firestore";
import { Store } from 'src/app/shared/store/store';
import { ClassRoom, School } from 'src/app/models/models';


@Injectable({
  providedIn: 'root'
})
export class ClassroomService {

  private classroomCollection: AngularFirestoreCollection

  constructor(
    private afs: AngularFirestore,
    private store: Store
  ) { 
    this.classroomCollection = afs.collection<ClassRoom>('classrooms')
  }

  get timestamp() {
    return firebase.firestore.FieldValue.serverTimestamp() as firebase.firestore.Timestamp
  }

  /**
   * Get all classrooms belong ton schoolConnected
   */
  get(): Observable<ClassRoom[]> {
     return this.store.select<School>('schoolConnected')
     .pipe(switchMap(school => this.afs.collection<ClassRoom>('classrooms', ref => ref.where('schoolId','==', school.schoolId))
    .snapshotChanges()
    .pipe(
      map(actions => actions.map(a => {
        const data = a.payload.doc.data()
        const id = a.payload.doc.id
        return {id, ...data} as ClassRoom
      }))
    )
    )
    )
  }

/**
 * add classroom on firestore database
 * @param data Observable<any>
 */
  add(data: any): Observable<any> {
    return this.store.select<School>('schoolConnected')
     .pipe(switchMap(school => of(this.classroomCollection.add({
      ...data,
      schoolId: school.schoolId,
      createdAt: this.timestamp,
      updatedAt: this.timestamp
    } as ClassRoom))))
  }

  /**
   * Get all classrooms belong ton schoolConnected
   */
  getBySchool(schoolId: string): Observable<ClassRoom[]> {
    return this.afs.collection<ClassRoom>('classrooms', ref => ref.where('schoolId','==', schoolId))
    .snapshotChanges(['added'])
    .pipe(
      map(actions => actions.map(a => {
        const data = a.payload.doc.data()
        const id = a.payload.doc.id
        return {id, ...data} as ClassRoom
      }))
    )
  }

   /**
   * Get all classrooms belong ton schoolConnected
   */
  getById(id: string): Observable<ClassRoom> {
    return this.store.select<School>('schoolConnected')
     .pipe(switchMap(school => this.afs.collection<ClassRoom>('classrooms', ref => ref.where('schoolId','==', school.schoolId))
     .doc(id)
     .get()
     .pipe(
       map(doc => ({id, ...doc.data()} as ClassRoom))
     ))
  )}

}
