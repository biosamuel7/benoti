import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { ClassRoom } from 'src/app/models/models';
import { ClassroomService } from '../../classroom/classroom.service';

@Injectable({providedIn: 'root'})
export class ClassRoomResolver implements Resolve<ClassRoom[]> {

    constructor(private service: ClassroomService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): ClassRoom[] | Observable<ClassRoom[]> | Promise<ClassRoom[]> {
        return this.service.get()
    }
    

}
