import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';

import * as firebase from 'firebase/app';
import "firebase/firestore";

import { map, switchMap } from 'rxjs/operators';
import { School, Member, BloomMember } from 'src/app/models/models';
import { Store } from 'src/app/shared/store/store';

@Injectable({
  providedIn: 'root'
})
export class MemberService {

  constructor(
    private afs: AngularFirestore,
    private store: Store
  ) { }

  get() {
    return this.store.select<School>('schoolConnected')
    .pipe(
      switchMap(school => this.afs.collection<Member>('members', ref => ref.where('schoolId','==', school.schoolId))
      .snapshotChanges(['added'])
      .pipe(
        map(actions => actions.map(a => {
          const data = a.payload.doc.data()
          const id = a.payload.doc.id
          return {id, ...data} as Member
        }))
      )
      )
    )
  }


  getById(id: string) {
    return this.store.select<School>('schoolConnected')
    .pipe(
      switchMap(school => this.afs.collection<Member>('members', ref => ref.where('schoolId','==',school.schoolId))
      .doc(id)
      .get()
      .pipe(
        map(doc => ({id, ...doc.data()} as Member))
      )
      )
    )
  }
  

  getAllMemberByClassroom(ids: string[]) {
    return this.store.select<School>('schoolConnected')
    .pipe(
      switchMap(school => this.afs.collection<Member>('members', ref => ref.where('schoolId','==',school.schoolId).where('classroom','array-contains-any',ids))
      .snapshotChanges(['added'])
      .pipe(
        map(actions => actions.map(a => {
          const data = a.payload.doc.data()
          const id = a.payload.doc.id
          return {id, ...data} as Member
        }))
      )
      )
    )
  }

  // getMemberByClassroom(id: string) {
  //   return this.store.select<School>('schoolConnected')
  //   .pipe(
  //     switchMap(school => this.afs.collection<Member>('members', ref => ref.where('schoolId','==',school.schoolId).where('classroom.classroomId','array-contains',id))
  //     .snapshotChanges()
  //     .pipe(
  //       map(actions => actions.map(a => {
  //         const data = a.payload.doc.data()
  //         const id = a.payload.doc.id
  //         return {id, ...data} as Member
  //       }))
  //     )
  //     )
  //   )
  // }

  add(data: Member | BloomMember){
      return this.afs.collection<Member | BloomMember>('members').add({
      ...data,
      createdAt: this.timestamp,
      updatedAt: this.timestamp
    })
  }

  deleteUserById(id: string) {
    return this.afs.collection<Member | BloomMember>('members').doc(id).delete()
  }

  get timestamp() { return firebase.firestore.FieldValue.serverTimestamp() as firebase.firestore.Timestamp}

}
 