import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { map, switchMap, tap } from 'rxjs/operators';
import { Billing, Formula, School } from 'src/app/models/models';
import { Store } from 'src/app/shared/store/store';

@Injectable({
  providedIn: 'root'
})
export class SchoolService {

  schoolCollection: AngularFirestoreCollection

  constructor(
    private afs: AngularFirestore,
    private store: Store
  ) { 
    this.schoolCollection = this.afs.collection<School>('schools')
  }

  getById(id: string): Observable<School>{
    return this.schoolCollection
    .doc(id)
    .get()
    .pipe(
      map(a => {
        const data = a.data() as Object;
        const schoolId = a.id
        return {schoolId, ...data} as School;
      }),
      switchMap((school: School) => {
        return this.schoolCollection.doc(school.schoolId).collection('billings', ref => ref.where('isActive','==',true)).get()
        .pipe(map(a => {
          let data = []
          a.forEach(elt => data.push(elt.data()))
          const billingActive = data.shift() as Billing
          return {...school, billingActive} as School
        }))
      })
      ,
      tap(school => this.store.set('schoolConnected',school))
    );
  }

  getActiveBilling(): Observable<Billing>{
    return this.store.select<School>('schoolConnected')
    .pipe(switchMap(school => {
      return this.schoolCollection
      .doc(school.schoolId)
      .collection('billings', ref => ref.where('isActive','==',true))
      .get()
      .pipe(map(a => {
        let data: Billing[] = []
        a.forEach(elt => data.push(elt.data() as Billing))
        const billingActive = data.shift() as Billing
        return billingActive
      }))
    })
   );
  }

  getSchoolFormula(): Observable<Formula>{
    return this.store.select<School>('schoolConnected')
    .pipe(switchMap(school => {
      return this.afs.collection<Formula>('formulas')
      .doc(school.billingActive.typeAccount)
      .get()
      .pipe(map(a => {
        return a.data() as Formula
      }))
    })
   );
  }

  updateSchool(data: School){
    return this.schoolCollection.doc(data.schoolId).update(data)
  }
}
