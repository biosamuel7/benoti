// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  apiKey: "AIzaSyAw80D8Fuz10lGiFvsmURHeifrmdgmdEEg",
  firebase: {
    apiKey: "AIzaSyD_FMBQn9bLpQ9RfzGMtYP8T5iZasdYJD4",
    authDomain: "benoti-app.firebaseapp.com",
    databaseURL: "https://benoti-app.firebaseio.com",
    projectId: "benoti-app",
    storageBucket: "benoti-app.appspot.com",
    messagingSenderId: "682373829105",
    appId: "1:682373829105:web:34fb3b85df796b55be3450",
    measurementId: "G-SX3Z8DSG5G",
    host:'',
    ssl: true,
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
